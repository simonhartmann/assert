package assert

// test helper

import (
	"reflect"
	"runtime"
	"runtime/debug"
	"testing"
)

// @todo:
// * cleanup callstack
// *

var (
	PrintStackTrace = true
)

func IsTrue(condition bool, t *testing.T) {
	assert(condition, t, 1)
}

func IsFalse(condition bool, t *testing.T) {
	assert(!condition, t, 1)
}

func IsNil(val interface{}, t *testing.T) {
	assert(val == nil, t, 1)
}

func IsNotNil(val interface{}, t *testing.T) {
	assert(val != nil, t, 1)
}

func AreEqual(val, tpl interface{}, t *testing.T) {
	assert(reflect.DeepEqual(val, tpl), t, 1)
}

func AreNotEqual(val, tpl interface{}, t *testing.T) {
	assert(!reflect.DeepEqual(val, tpl), t, 1)
}

func Fail(t *testing.T) {
	assert(false, t, 1)
}

func assert(condition bool, t *testing.T, calldepth int) {
	if !condition {
		_, file, line, _ := runtime.Caller(calldepth + 1)
		t.Errorf("%s:%d", file, line)
		if PrintStackTrace {
			debug.PrintStack()
		}
		t.FailNow()
	}
}
